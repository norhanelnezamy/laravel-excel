# Laravel Excel


## Installation and configuration steps

```bash
git clone git@gitlab.com/norhanelnezamy/laravel-excel.git
```

```bash
cp -i .env.example .env
```
```bash
php artisan key:generate
```

Please note to set database connection and smtp configuration in ur .env file 

```bash
composer update
```

```bash
php artisan serve
```

```bash
php artisan queue:work
```

## Usage

Use [Landing](http://127.0.0.1:8000) to landing page.

## For UnitTest

```bash
./vendor/bin/phpunit
```
