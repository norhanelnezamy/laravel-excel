<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use File;
use Excel;
class UsersImportTest extends TestCase
{

    protected function getTestFile($file_name, $path)
    {

        $file = new UploadedFile(
            $path,
            $file_name,
            File::mimeType($path),
            null,
            true
        );

        return ['file' => $file];
    }


    /** 
     * 
     * @test 
     * 
     * */
    public function user_can_queue_the_users_import() 
    {

        $file_name = 'users-3.xlsx';
        // $file_name = '100K-Morshedy-task(Autosaved).xlsx';
        $file_path = base_path('public/'.$file_name);

        $this->postJson(route('file.import'), $this->getTestFile($file_name, $file_path))
        ->assertSuccessful();

    }
    
}
