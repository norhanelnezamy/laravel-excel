<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImportRequest;
use App\Imports\UsersImport;
// use App\Jobs\UsersImportNotify;

class IndexController extends Controller
{

    public function index()
    {
        return view('index');
    }


    public function import(ImportRequest $request)
    {
      $import = new UsersImport;

      // $import->queue($request->file('file'))->chain([
      //   new UsersImportNotify($import->getAcceptedCount(), $import->getRejectedCount()),
      // ]);

      $import->queue($request->file('file'));

      return redirect()->back()->with('msg', 'File imported successfully .');

    }


}
