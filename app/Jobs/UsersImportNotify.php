<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use \Maatwebsite\Excel\Reader;

class UsersImportNotify implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $accepted_count ;
    
    protected $rejected_count ;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($accepted_count, $rejected_count)
    {
        $this->accepted_count  = $accepted_count;
        $this->rejected_count  = $rejected_count;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Mail::send('mail', ['rejected_count' => $this->rejected_count, 'accepted_count'=>$this->accepted_count], function ($message){
            $message->to('norhanelnezamy@gmail.com')->subject('Importing Users');
          });
    }
}
