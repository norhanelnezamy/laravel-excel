<?php 

namespace App\Imports;

use App\User;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\Importable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use App\Jobs\UsersImportNotify;

class UsersImport implements  ToModel, WithHeadingRow , WithChunkReading, ShouldQueue, WithValidation, SkipsOnFailure , SkipsOnError, WithEvents
{
    use Importable , SkipsFailures, SkipsErrors, RegistersEventListeners ;

    protected $accepted_count ;

    protected $rejected_count ;
    

    public function __construct()
    {
        \Config::set('queue.counter', ['accepted'=>0, 'rejected'=>0]);
        // $this->$accepted_count = 0;
        // $this->$rejected_count = 0;
    }

    public function registerEvents(): array
    {
        return [
            AfterImport::class => function(AfterImport $event) {
                UsersImportNotify::dispatch($this->getCounter('accepted'), $this->getCounter('rejected'));
            }          
        ];
    }

    public function model(array $row)
    {
        $this->increaseCounter('accepted', 1);

        return new User([
            'name'     => str_random(6),
            'first_name'     => $row['first_name'],
            'second_name'    => $row['second_name'],
            'family_name' => $row['family_name'],
            'uid' => $row['uid'],
            'password' => Hash::make(str_random(6)),
        ]);
    }

    public function rules(): array
    {
        return [
            '*.first_name' => 'required',
            '*.second_name' => 'required',
            '*.family_name' => 'required',
            '*.uid' => 'required',
        ];
    }

    /**
     * @param Failure ...$failures
     */
    public function onFailure(Failure ...$failures)
    {
        $this->increaseCounter('rejected', count($failures));
        //  $this->rejected_count +=  count($failures);
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function increaseCounter($key,$increase)
    {
        $counter = \Config::get('queue.counter');
        $counter[$key] += $increase;
        \Config::set('queue.counter' ,$counter);
        // $this->{$key} += $increase ;
    }

    public function getCounter($key): int
    {
        $counter = \Config::get('queue.counter');
        return $counter[$key];
        // return $this->{$key};
    }

}


?>